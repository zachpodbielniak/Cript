#   ____      _       _   
#  / ___|_ __(_)_ __ | |_ 
# | |   | '__| | '_ \| __|
# | |___| |  | | |_) | |_ 
#  \____|_|  |_| .__/ \__|
#              |_|        
# 
# Pseudo-Scripting In C.
# Copyright (C) 2020 Zach Podbielniak
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)
#CC_FLAGS_D += -fsanitize=address

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall



FILES = Cript.o
FILES_D = Cript_d.o




all:	bin libcript.so
debug:	bin libcript_d.so 
host:	bin cript
host_debug: bin cript_d
test: 	bin $(TEST)

bin:
	mkdir -p bin/

clean: 	bin
	rm -rf bin/

check:	bin 
	cppcheck . --std=c89 -j $(shell nproc) --inline-suppr --error-exitcode=1



# Install

install:
	cp bin/libcript.so /usr/lib/

install_debug:
	cp bin/libcript_d.so /usr/lib/

install_headers:
	rm -rf /usr/include/Cript/
	mkdir -p /usr/include/Cript
	find . -type f -name "*.h" -exec install -D {} /usr/include/Cript/{} \;
	mv /usr/include/Cript/Src/* /usr/include/Cript
	rm -rf /usr/include/Cript/Src



libcript.so: $(FILES)
	$(CC) -shared -fPIC -s -o bin/libcript.so bin/*.o -pthread -lpodnet $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o

libcript_d.so: $(FILES_D)
	$(CC) -g -shared -fPIC -o bin/libcript_d.so bin/*_d.o -pthread -lpodnet_d $(STD)
	rm bin/*.o

cript:
	$(CC) -o bin/cript Src/Main.c -lpodnet -lcript $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH)

cript_d:
	$(CC) -g -o bin/cript Src/Main.c -lpodnet_d -lcript_d $(CC_FLAGS_D)




Cript.o: 
	$(CC) -fPIC -c -o bin/Cript.o Src/Cript.c $(CC_FLAGS)

Cript_d.o: 
	$(CC) -g -fPIC -c -o bin/Cript_d.o Src/Cript.c $(CC_FLAGS_D)
