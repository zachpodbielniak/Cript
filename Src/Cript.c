/*
  ____      _       _   
 / ___|_ __(_)_ __ | |_ 
| |   | '__| | '_ \| __|
| |___| |  | | |_) | |_ 
 \____|_|  |_| .__/ \__|
             |_|        

Pseudo-Scripting In C.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "Cript.h"


typedef LONG (*LPFN_MAIN_PROC)(
	LONG 		lArgCount,
	DLPSTR 		dlpszArgValues
);




typedef struct __CRIPT
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hModule;
	HANDLE 		hFile;

	LPSTR 		lpszSourceFile;
	LPSTR 		lpszModdedSourceFile;
	LPSTR 		lpszModuleFile;
	LPSTR 		lpszOptionalFlagsInherit;
	LPSTR 		lpszOptionalFlagsParam;

	ULONGLONG	ullCrc;
	ULONG 		ulFlags;

	LPFN_MAIN_PROC	lpfnMain;
} CRIPT, *LPCRIPT;



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__FigureOutInheritedParamsAndModifyFile(
	_In_ 		HANDLE 			hFile,
	_Out_ 		DLPSTR 			dlpszOutParams,
	_Out_ 		DLPSTR 			dlpszOutModdedFile
){
	EXIT_IF_UNLIKELY_NULL(hFile, FALSE);
	EXIT_IF_UNLIKELY_NULL(dlpszOutParams, FALSE);

	HANDLE hModdedFile;
	CSTRING csBuffer[8192];
	CSTRING csParams[8192];
	CSTRING csModdedFile[512];
	LPSTR lpszBuffer;
	LPSTR lpszTest;
	LPSTR lpszParams;
	ARCHLONG alRead;
	UARCHLONG ualIndex;
	BOOL bRet;

	lpszBuffer = (LPSTR)csBuffer;
	ualIndex = 0;
	bRet = FALSE;
	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csModdedFile, sizeof(csModdedFile));

	/* Create Modded File Name */
	StringPrintFormatSafe(
		csModdedFile,
		sizeof(csModdedFile) - 1,
		"/tmp/cript-mod-%lu.c",
		(UARCHLONG)getpid()
	);

	/* Create Modified File */
	hModdedFile = CreateFile(csModdedFile, FILE_PERMISSION_CREATE | FILE_PERMISSION_WRITE, 0);
	if (NULL_OBJECT == hModdedFile)
	{ return FALSE; }

	*dlpszOutModdedFile = GlobalAllocAndZero(StringLength(csModdedFile) + 0x01U);
	StringCopy(*dlpszOutModdedFile, csModdedFile);


	while (-1 != (alRead = ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	))){
		lpszTest = StringInString(lpszBuffer, "#define CRIPT_PARAMS");

		if (NULLPTR == lpszTest)
		{ 
			if (0 != ualIndex)
			{ WriteFile(hModdedFile, lpszBuffer, 0); }
			ZeroMemory(csBuffer, sizeof(csBuffer) - 1);
			ualIndex++;
			continue;
		}

		/* This line is a prospect */
		lpszParams = StringInString(lpszTest, "\"");
		if (NULLPTR == lpszParams)
		{ 
			if (0 != ualIndex)
			{ WriteFile(hModdedFile, lpszBuffer, 0); }
			ZeroMemory(csBuffer, sizeof(csBuffer) - 1);
			ualIndex++;
			continue;
		}

		if (0 != ualIndex)
		{ WriteFile(hModdedFile, lpszBuffer, 0); }
		ualIndex++;

		/* This is the correct settings */
		lpszParams = (LPSTR)((UARCHLONG)lpszParams + 0x01U);
		ZeroMemory(csParams, sizeof(csParams));
		StringCopySafe(csParams, lpszParams, sizeof(csParams) - 1);

		/* Remove ending newline and ending quote */
		StringReplaceCharacter(csParams, '\n', '\0');
		StringReplaceCharacter(csParams, '\"', '\0');

		*dlpszOutParams = GlobalAllocAndZero(StringLength(csParams) + 0x01U); 
		if (NULLPTR == *dlpszOutParams)
		{ return FALSE; }

		StringCopy(*dlpszOutParams, csParams);
		bRet = TRUE;
	}

	DestroyObject(hModdedFile);
	return bRet;
}


_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__CalculateFileHash(
	_In_		LPCRIPT			lpcrData
){
	LPSTR lpszFileContents;
	UARCHLONG ualLength;

	RewindFile(lpcrData->hFile);
	ReadWholeFile(lpcrData->hFile, &lpszFileContents, FILE_READ_ALLOC_OUT);

	ualLength = StringLength(lpszFileContents);
	lpcrData->ullCrc = Crc64((LPBYTE)lpszFileContents, ualLength, 0);

	FreeMemory(lpszFileContents);
	return TRUE;
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyCript(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPCRIPT lpcrData;
	struct stat statData;
	lpcrData = hDerivative;

	DestroyObject(lpcrData->hModule);
	DestroyObject(lpcrData->hFile);
	
	/* Delete the modded C file */
	if (NULLPTR != lpcrData->lpszModdedSourceFile && !(lpcrData->ulFlags & CRIPT_PRESERVE_SOURCE))
	{
		if (-1 != stat(lpcrData->lpszModdedSourceFile, &statData))
		{ unlink(lpcrData->lpszModdedSourceFile); }
	}
	FreeMemory(lpcrData->lpszModdedSourceFile);
	FreeMemory(lpcrData->lpszModuleFile);
	FreeMemory(lpcrData->lpszOptionalFlagsInherit);
	FreeMemory(lpcrData->lpszOptionalFlagsParam);
	FreeMemory(lpcrData->lpszSourceFile);

	FreeMemory(lpcrData);
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CRIPT_API
HANDLE 
CreateCript(
	_In_Z_ 		LPCSTR 			lpcszFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszOptionalCompilationFlags,
	_In_Opt_ 	ULONG 			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(lpcszFile, NULL_OBJECT);
	UNREFERENCED_PARAMETER(ulFlags);

	HANDLE hCript;
	LPCRIPT lpcrData;
	BOOL bGotParams;
	CSTRING csBuffer[8192];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	lpcrData = GlobalAllocAndZero(sizeof(CRIPT));
	if (NULLPTR == lpcrData)
	{ JUMP(__CRIPT_ALLOC_FAIL); }

	lpcrData->lpszSourceFile = GlobalAllocAndZero(StringLength(lpcszFile) + 0x01U);
	if (NULLPTR == lpcrData->lpszSourceFile)
	{ JUMP(__SOURCE_FILE_ALLOC_FAIL);}

	lpcrData->lpszOptionalFlagsParam = GlobalAllocAndZero(StringLength(lpcszOptionalCompilationFlags) + 0x01U);
	if (NULLPTR == lpcrData->lpszOptionalFlagsParam)
	{ JUMP(__OPTIONAL_FLAG_ALLOC_FAIL); }

	lpcrData->hFile = OpenFile(lpcszFile, FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == lpcrData->hFile)
	{ JUMP(__OPEN_FILE_FAIL); }

	lpcrData->ulFlags = ulFlags;

	bGotParams = __FigureOutInheritedParamsAndModifyFile(
		lpcrData->hFile, 
		&(lpcrData->lpszOptionalFlagsInherit),
		&(lpcrData->lpszModdedSourceFile)
	);

	if (FALSE == bGotParams)
	{
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			#ifdef __DEBUG__
			"-g -lpodnet_d %s",
			#else
			"-lpodnet %s",
			#endif
			lpcszOptionalCompilationFlags
		);
	}
	else 
	{
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			#ifdef __DEBUG__
			"-g -lpodnet_d %s %s",
			#else
			"-lpodnet %s %s",
			#endif
			lpcszOptionalCompilationFlags,
			lpcrData->lpszOptionalFlagsInherit
		);
	}

	if (!(ulFlags & CRIPT_FORCE_COMPILE))
	{ __CalculateFileHash(lpcrData); }

	lpcrData->hModule = CompileAndLoadModuleCached(
		lpcrData->lpszModdedSourceFile,
		csBuffer,
		NULLPTR,
		NULLPTR,
		LOCK_TYPE_MUTEX,
		0,
		NULLPTR,
		lpcrData->ullCrc,
		TRUE
	);

	if (NULL_OBJECT == lpcrData->hModule)
	{ JUMP(__COMPILE_MODULE_FAIL); }

	lpcrData->lpfnMain = GetModuleRawProcByName(lpcrData->hModule, "main");
	if (NULLPTR == lpcrData->lpfnMain)
	{ 
		WriteFile(GetStandardError(), "There is no main() defined!\n", 0);
		JUMP(__MODULE_NO_MAIN); 
	}

	hCript = CreateHandleWithSingleInheritor(
		lpcrData,
		&(lpcrData->hThis),
		HANDLE_TYPE_CRIPT,
		__DestroyCript,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpcrData->ullId),
		0
	);

	if (NULL_OBJECT == hCript)
	{ JUMP(__OBJECT_CREATION_FAIL); }

	return hCript;

	__OBJECT_CREATION_FAIL:
	__MODULE_NO_MAIN:
	DestroyObject(lpcrData->hModule);
	__COMPILE_MODULE_FAIL:
	if (NULLPTR != lpcrData->lpszOptionalFlagsInherit)
	{ FreeMemory(lpcrData->lpszOptionalFlagsInherit); }
	__OPEN_FILE_FAIL:
	FreeMemory(lpcrData->lpszOptionalFlagsParam);
	__OPTIONAL_FLAG_ALLOC_FAIL:
	FreeMemory(lpcrData->lpszSourceFile);
	__SOURCE_FILE_ALLOC_FAIL:
	FreeMemory(lpcrData);
	__CRIPT_ALLOC_FAIL:
	return NULL_OBJECT;
}




_Success_(return != 255, _Interlocked_Operation_)
CRIPT_API
LONG
ExecuteCript(
	_In_ 		HANDLE 			hCript,
	_In_		DLPSTR 			dlpszArgs,
	_In_	 	LONG 			lArgCount
){
	EXIT_IF_UNLIKELY_NULL(hCript, 255);
	EXIT_IF_UNLIKELY_NULL(dlpszArgs, 255);
	EXIT_IF_UNLIKELY_NULL(lArgCount, 255);

	LPCRIPT lpcrData;
	lpcrData = OBJECT_CAST(hCript, LPCRIPT);

	return lpcrData->lpfnMain(lArgCount, dlpszArgs);
}