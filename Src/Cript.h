/*
  ____      _       _   
 / ___|_ __(_)_ __ | |_ 
| |   | '__| | '_ \| __|
| |___| |  | | |_) | |_ 
 \____|_|  |_| .__/ \__|
             |_|        

Pseudo-Scripting In C.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#ifndef CRIPT_H
#define CRIPT_H



#include "Prereqs.h"
#include "Defs.h"



typedef enum __CRIPT_FLAGS
{
	CRIPT_FORCE_COMPILE		= 1 << 0,
	CRIPT_PRESERVE_SOURCE		= 1 << 1
} CRIPT_FLAGS;




_Success_(return != NULL_OBJECT, _Non_Locking_)
CRIPT_API
HANDLE 
CreateCript(
	_In_Z_ 		LPCSTR 			lpcszFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszOptionalCompilationFlags,
	_In_Opt_ 	ULONG 			ulFlags
);




_Success_(return != 255, _Interlocked_Operation_)
CRIPT_API
LONG
ExecuteCript(
	_In_ 		HANDLE 			hCript,
	_In_		DLPSTR 			dlpszArgs,
	_In_	 	LONG 			lArgCount
);


#endif