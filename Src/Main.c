/*
  ____      _       _   
 / ___|_ __(_)_ __ | |_ 
| |   | '__| | '_ \| __|
| |___| |  | | |_) | |_ 
 \____|_|  |_| .__/ \__|
             |_|        

Pseudo-Scripting In C.
Copyright (C) 2020-2021 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "Cript.h"
#include <PodNet/PodNet.h>
#include <PodNet/CString/CHeapString.h>
#include <sys/stat.h>




typedef struct __STATE
{
	DLPSTR 		dlpszArgValues;
	LONG 		lArgCount;

	BOOL		bCriptFileStringAlloc;
	LPSTR 		lpszCriptFile;
	ULONG		ulFlags;

	BOOL		bInlineMode;
	LPSTR		lpszInlineCode;
	LPSTR		lpszIncludes;

	BOOL		bStdIn;
	LPSTR		lpszStdInCode;

	UARCHLONG	ualNumberOfIncludes;
	DLPSTR		dlpszIncludes;

	HVECTOR		hvParams;
	HVECTOR		hvPreloads;
} STATE, *LPSTATE;



_Kills_Process_
KILL 
__PrintHelp(
	VOID 
){
	PrintFormat("Cript -- Pseudo Scripting In C\n");
	PrintFormat("------------------------------\n");
	PrintFormat("License: AGLv3 - Copyright (C) 2020-2021 Zach Podbielniak\n");
	PrintFormat("Usage: cript [Cript_Params] <C_Source_File> [C_Script_Parameters]\n");
	PrintFormat("Cript Parameters:\n");
	PrintFormat("\t-h,--help\t\tShow this help message\n");
	PrintFormat("\t-n,--no-cache\t\tDo not cache the compiled module. This forces JIT compilation on each run\n");
	PrintFormat("\t-S,--source-preserve\tPreserve the modified source file under /tmp\n");
	PrintFormat("\t-i,--inline\t\tAllow inline C code to be executed\n");
	PrintFormat("\t-I,--include\t\tInclude. By default, stdio.h, stdlib.h, and string.h are included. Separated by ';'\n");
	PrintFormat("\t-p,--preload\t\tPreload the Cript with a library, controlling in which order libraries are loaded\n");
	PrintFormat("\t-,--stdin\t\tRead from stdin until EOF. Can be used with -i,--inline.\n");
	PostQuitMessage(0);
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ParseArgs(
	_In_ 		LPSTATE 	lppsState
){
	LPSTR lpszKey;
	LPSTR lpszValue;
	ARCHLONG alIndex;
	BOOL bGotFile; /* Used to determine if we are parsing args for Cript or the child */
	struct stat statFile;
	CSTRING csFile[0x400];

	lpszKey = NULLPTR;
	lpszValue = NULLPTR;
	bGotFile = FALSE;

	for (
		alIndex = 1;
		alIndex < (ARCHLONG)lppsState->lArgCount;
		alIndex++
	){
		ZeroMemory(csFile, sizeof(csFile));
		lpszKey = lppsState->dlpszArgValues[alIndex];
		if (1 + alIndex == lppsState->lArgCount)
		{ lpszValue = NULLPTR; }
		else 
		{ lpszValue = lppsState->dlpszArgValues[alIndex + 1]; }


		/* Not a file, we are still processing Cript args */
		if (TRUE == bGotFile)
		{ JUMP(__GOT_FILE); }
		else if (-1 == stat(lpszKey, &statFile))
		{
			if (
				0 == StringCompare(lpszKey, "-h") ||
				0 == StringCompare(lpszKey, "--help")
			){ __PrintHelp(); }

			else if (
				0 == StringCompare(lpszKey, "-n") ||
				0 == StringCompare(lpszKey, "--no-cache")
			){ lppsState->ulFlags |= CRIPT_FORCE_COMPILE; }

			else if (
				0 == StringCompare(lpszKey, "-S") ||
				0 == StringCompare(lpszKey, "--source-preserve")
			){ lppsState->ulFlags |= CRIPT_PRESERVE_SOURCE; }

			else if (
				0 == StringCompare(lpszKey, "-i") ||
				0 == StringCompare(lpszKey, "--inline")
			){ 
				lppsState->bInlineMode = TRUE;
				lppsState->lpszInlineCode = lpszValue;

				if (NULLPTR == lpszValue)
				{ lppsState->bStdIn = TRUE; }
			}

			else if (
				0 == StringCompare(lpszKey, "-I") ||
				0 == StringCompare(lpszKey, "--include")
			){ lppsState->lpszIncludes = lpszValue; }

			else if (
				0 == StringCompare(lpszKey, "-p") ||
				0 == StringCompare(lpszKey, "--preload")
			){
				HANDLE hModule;
				if (NULLPTR == lpszValue)
				{ break; }

				hModule = LoadModule(
					(LPCSTR)lpszValue,
					NULLPTR,
					NULLPTR,
					LOCK_TYPE_SPIN_LOCK,
					0,
					NULLPTR
				);

				if (NULLPTR == lppsState->hvPreloads)
				{ lppsState->hvPreloads = CreateVector(0x08, sizeof(HANDLE), 0); }

				if (NULL_OBJECT != hModule)
				{ VectorPushBack(lppsState->hvPreloads, &hModule, 0); }
				else 
				{
					WriteFile(GetStandardError(), "Unable to open file: ", 0);
					WriteFile(GetStandardError(), lpszValue, 0);
					WriteFile(GetStandardError(), "\n", 0);
					PostQuitMessage(1);
				}
			}

			else if (
				0 == StringCompare(lpszKey, "-") ||
				0 == StringCompare(lpszKey, "--stdin")
			){ lppsState->bStdIn = TRUE; }
		}
		else 
		{ 
			if (FALSE == bGotFile)
			{ lppsState->lpszCriptFile = lpszKey; }

			__GOT_FILE:
			bGotFile = TRUE;

			if (NULLPTR == lppsState->hvParams)
			{ lppsState->hvParams = CreateVector(0x08, sizeof(LPSTR), 0); } 

			VectorPushBack(lppsState->hvParams, &lpszKey, 0);
		}
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GenerateInlineBoilerPlate(
	_In_		LPSTATE		lppsState
){
	EXIT_IF_LIKELY_NULL(lppsState, FALSE);
	EXIT_IF_LIKELY_VALUE(lppsState->bInlineMode, FALSE, FALSE);
	EXIT_IF_NULL(lppsState->lpszInlineCode, FALSE);

	HANDLE hFile;
	ARCHLONG alIterator;
	LPSTR lpszBaseStart;
	LPSTR lpszBaseEnd;
	LPSTR lpszFile;
	CSTRING csBuffer[0x100];
	CSTRING csFile[PATH_MAX];
	HSTRING hsFinal;

	ZeroMemory(csFile, sizeof(csFile));
	lpszFile = (LPSTR)csFile;

	lpszBaseStart = "LONG\n" \
			"Main(\n" \
			"	_In_		LONG		lArgCount,\n" \
			"	_In_Opt_Z_	DLPSTR		dlpszArgValues\n" \
			"){\n";

	lpszBaseEnd = "return 0;\n}\n";

	hsFinal = CreateHeapString("#define PODNET_BASICS\n");
	HeapStringAppend(hsFinal, "#include <PodNet/PodNet.h>\n");

	lppsState->ualNumberOfIncludes = StringSplit(
		lppsState->lpszIncludes,
		";",
		&(lppsState->dlpszIncludes)
	);

	for (
		alIterator = 0;
		alIterator < (ARCHLONG)lppsState->ualNumberOfIncludes;
		alIterator++
	){
		ZeroMemory(csBuffer, sizeof(csBuffer));
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"#include <%s>\n",
			lppsState->dlpszIncludes[alIterator]
		);

		HeapStringAppend(hsFinal, (LPCSTR)csBuffer);
	}

	HeapStringAppend(hsFinal, lpszBaseStart);
	HeapStringAppend(hsFinal, lppsState->lpszInlineCode);
	HeapStringAppend(hsFinal, lpszBaseEnd);

	StringPrintFormatSafe(
		csFile,
		sizeof(csFile) - 1,
		"/tmp/cript-inline-%u",
		getpid()
	);

	hFile = CreateFile((LPCSTR)csFile, FILE_PERMISSION_CREATE | FILE_PERMISSION_READ, 0);
	WriteFile(hFile, (LPCSTR)HeapStringValue(hsFinal), 0);
	DestroyObject(hFile);
	DestroyHeapString(hsFinal);

	lppsState->lpszCriptFile = StringDuplicate(csFile);
	lppsState->bCriptFileStringAlloc = TRUE;

	if (NULLPTR == lppsState->hvParams)
	{ lppsState->hvParams = CreateVector(0x08, sizeof(LPSTR), 0); }

	VectorPushBack(lppsState->hvParams, &lpszFile, 0);

	return TRUE;
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ReadStdIn(
	_In_		LPSTATE		lppsState
){
	EXIT_IF_UNLIKELY_NULL(lppsState, FALSE);

	HANDLE hStdIn;
	HSTRING hsStdIn;
	CSTRING csBuffer[512];
	LPSTR lpszBuffer;

	hStdIn = GetStandardIn();
	hsStdIn = NULLPTR;
	ZeroMemory(csBuffer, sizeof(csBuffer));
	lpszBuffer = (LPSTR)csBuffer;

	while (-1 != ReadLineFromFile(hStdIn, &lpszBuffer, sizeof(csBuffer) - 1, 0))
	{
		if (NULLPTR == hsStdIn)
		{ hsStdIn = CreateHeapString((LPCSTR)lpszBuffer); }
		else 
		{ HeapStringAppend(hsStdIn, (LPCSTR)lpszBuffer); }
	}

	lppsState->lpszStdInCode = StringDuplicate(HeapStringValue(hsStdIn));
	DestroyHeapString(hsStdIn);

	if (lppsState->bInlineMode)
	{ lppsState->lpszInlineCode = StringDuplicate(lppsState->lpszStdInCode); }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__Cleanup(
	_In_		LPSTATE		lppsState
){
	EXIT_IF_UNLIKELY_NULL(lppsState, FALSE);

	if (NULLPTR != lppsState->hvParams)
	{ DestroyVector(lppsState->hvParams); }

	if (TRUE == lppsState->bCriptFileStringAlloc)
	{ FreeMemory(lppsState->lpszCriptFile); }

	if (NULLPTR != lppsState->hvPreloads)
	{
		ARCHLONG alIterator;
		HANDLE hIterator;

		for (
			alIterator = 0;
			alIterator < (ARCHLONG)VectorSize(lppsState->hvPreloads);
			alIterator++
		){
			hIterator = *(LPHANDLE)VectorAt(lppsState->hvPreloads, alIterator);
			DestroyObject(hIterator);
		}
	}

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	HANDLE hCript;
	LONG lRetCode;
	STATE stProgram;

	ZeroMemory(&stProgram, sizeof(stProgram));

	stProgram.lArgCount = lArgCount;
	stProgram.dlpszArgValues = dlpszArgValues;

	__ParseArgs(&stProgram);

	if (TRUE == stProgram.bStdIn)
	{ __ReadStdIn(&stProgram); }

	if (TRUE == stProgram.bInlineMode)
	{ __GenerateInlineBoilerPlate(&stProgram); }

#ifdef __DEBUG__
	hCript = CreateCript(
		stProgram.lpszCriptFile,
		"-lpodnet_d",
		stProgram.ulFlags
	);
#else 
	hCript = CreateCript(
		stProgram.lpszCriptFile,
		"-lpodnet",
		stProgram.ulFlags
	);
#endif

	lRetCode = ExecuteCript(hCript, VectorAt(stProgram.hvParams, 0), VectorSize(stProgram.hvParams));
	DestroyObject(hCript);

	__Cleanup(&stProgram);

	return lRetCode;
}
