#!/sbin/cript 

/*
  ____      _       _   
 / ___|_ __(_)_ __ | |_ 
| |   | '__| | '_ \| __|
| |___| |  | | |_) | |_ 
 \____|_|  |_| .__/ \__|
             |_|        

Pseudo-Scripting In C.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "PodNet/PodNet.h"
#include "PodNet/CScripting/CShell.h"
#include "PodNet/CModule/CModule.h"



LONG 
Main(
	_In_		LONG		lArgCount,
	_In_Z_		DLPSTR		dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	LPSTR lpszOutput;
	LPSTR lpszTemp;

	Bash("cat BashReplacement.c | grep Cript", &lpszOutput);
	if (1 < lArgCount)
	{ Bash("echo '%s'", NULLPTR, dlpszArgValues[1]); }

	Bash(
		"echo '%p'", 
		NULLPTR, 
		LoadModule(
			"libpodnet.so",
			NULLPTR,
			NULLPTR,
			LOCK_TYPE_SPIN_LOCK,
			0,
			NULLPTR
		)
	);
	
	Bash(
		"echo '%p'", 
		NULLPTR, 
		LoadModule(
			"libcrock.so",
			NULLPTR,
			NULLPTR,
			LOCK_TYPE_SPIN_LOCK,
			0,
			NULLPTR
		)
	);

	Bash(
		"echo '%p'",
		NULLPTR,
		GetCurrentThread()
	);


	FreeMemory(lpszTemp);
	FreeMemory(lpszOutput);
	return 0;
}

