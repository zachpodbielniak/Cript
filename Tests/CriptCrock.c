#!/usr/bin/cript

/*
  ____      _       _   
 / ___|_ __(_)_ __ | |_ 
| |   | '__| | '_ \| __|
| |___| |  | | |_) | |_ 
 \____|_|  |_| .__/ \__|
             |_|        

Pseudo-Scripting In C.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


/* This requires that CRock is installed */
#define CRIPT_PARAMS "-lcrock_d"

#include <CRock/CRock.h>

_Call_Back_
HTTP_STATUS_CODE
__Root(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	LPQUERYSTRING_DATA lpqsdInfo;
	UARCHLONG ualIndex;
	CSTRING csBody[8192];
	CSTRING csTemp[512];

	LPSTR lpszBegin;
	LPSTR lpszEnd;

	ZeroMemory(csBody, sizeof(csBody));
	ZeroMemory(csTemp, sizeof(csTemp));

	PrintFormat("/ was called\n");
	PrintFormat("QueryString: %s\n", lprdData->lpszRequestParams);
	ProcessQueryStringParameters(lprdData->hRequest);

	lpszBegin = "<html><head><title>CRock Homepage</title><body>";
	lpszEnd = "</body></html>";

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvQueryParameters);
		ualIndex++
	){
		lpqsdInfo = VectorAt(lprdData->hvQueryParameters, ualIndex);

		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>%s</h1><br /><h4>%s</h4><br /><br />",
			lpqsdInfo->lpszKey,
			lpqsdInfo->lpszValue
		);

		StringConcatenateSafe(csBody, csTemp, sizeof(csBody) - 1);
	}


	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");


	RequestAddResponse(lprdData->hRequest, lpszBegin, StringLength(lpszBegin));
	RequestAddResponse(lprdData->hRequest, csBody, StringLength(csBody));
	RequestAddResponse(lprdData->hRequest, lpszEnd, StringLength(lpszEnd));
	return HTTP_STATUS_200_OK;
}

LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hHttpServer;
	DLPSTR dlpszBindAddresses;
	LPUSHORT lpusBindPorts;

	dlpszBindAddresses = GlobalAllocAndZero(sizeof(LPSTR));
	lpusBindPorts = GlobalAllocAndZero(sizeof(USHORT));

	dlpszBindAddresses[0] = "0.0.0.0";
	lpusBindPorts[0] = 9998;




	hHttpServer = CreateHttpServerEx(
		"testsrv01",
		(DLPCSTR)dlpszBindAddresses,
		1,
		lpusBindPorts,
		1,
		"testsrv01",
		65536,
		16,
		0
	);

	DefineHttpUriProc(
		hHttpServer,
		"/",
		HTTP_METHOD_GET,
		__Root,
		NULLPTR
	);

	LongSleep(INFINITE_WAIT_TIME);
	return 0;
}

