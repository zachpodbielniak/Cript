#!/sbin/cript 

/*
  ____      _       _   
 / ___|_ __(_)_ __ | |_ 
| |   | '__| | '_ \| __|
| |___| |  | | |_) | |_ 
 \____|_|  |_| .__/ \__|
             |_|        

Pseudo-Scripting In C.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#define CRIPT_PARAMS "-Ofast"

#include "PodNet/PodNet.h"
#include "PodNet/CScripting/CShell.h"


_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__IsPrime(
	_In_		UARCHLONG	ualNumber
){
	UARCHLONG ualIterator;
	BOOL bIsPrime;

	bIsPrime = FALSE;

	if (1 == ualNumber || 0 >= ualNumber)
	{ return FALSE; }
	else if (2 == ualNumber)
	{ return TRUE; }
	else 
	{
		for (
			ualIterator = 2;
			ualIterator <= ualNumber / 2;
			ualIterator++
		){
			if (0 == ualNumber % ualIterator)
			{
				bIsPrime = TRUE;
				JUMP(__GOT_IT);
			}
		}
	}

	__GOT_IT:
	return bIsPrime;
}



LONG 
Main(
	_In_		LONG		lArgCount,
	_In_Z_		DLPSTR		dlpszArgValues
){
	UARCHLONG ualIterator;
	UARCHLONG ualMax;

	if (lArgCount < 2)
	{ 
		PrintFormat("Specify Range");
		return 1; 
	}

	ualMax = (UARCHLONG)StringToArchLong(dlpszArgValues[1]);

	for (
		ualIterator = 1;
		ualIterator <= ualMax;
		ualIterator++
	){ __IsPrime(ualIterator); }

	return 0;
}